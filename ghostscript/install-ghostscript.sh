#!/bin/bash

which gs || (
  which apt && ( apt update && apt install -y ghostscript )
  which apk && (           apk add --no-cache ghostscript )
)

which convert && ( 
ls -1 /etc/ImageMagick-*/policy.xml|while read impolicy;do 
  for filetype in EPS PDF XPS PS PS2 PS3;do 
    grep 'policy domain="coder" rights="read | write" pattern="'$filetype'"' "$impolicy" || ( 
        sed 's~</policymap>~<policy domain="coder" rights="read | write" pattern="'$filetype'" />\n</policymap>~g' "$impolicy" -i ;done 
        )
  done
)
